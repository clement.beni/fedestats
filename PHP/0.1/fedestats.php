<?php

function showLicences($tab_licences){
	foreach($tab_licences as $elt_N1) {
		$txt_N2 = "" ;
		$tmp_coma = 0 ;
		foreach($elt_N1 as $elt_N2) {
			$txt_N2 .= $elt_N2 ;
			if($tmp_coma < 3) {
				$txt_N2 .= ', ' ;
			}
			$tmp_coma += 1 ;
		}
		print_r($txt_N2) ;
		echo '<br />' ;
	}
}


function listOfLicencesNumber($tab_licences) {
	$list_licenceNumber = [] ;
	foreach($tab_licences as $elt_licence) {
		if (in_array($elt_licence[0], $list_licenceNumber) == false) {
			array_push($list_licenceNumber, $elt_licence[0]) ;
		}
	}
	return $list_licenceNumber ; //$txt_result ;
}


function listOfYears($tab_licences) {
	$list_years = [] ;
	foreach($tab_licences as $elt_licence) {
		if (in_array($elt_licence[1], $list_years) == false) {
			array_push($list_years, $elt_licence[1]) ;
		}
	}
	return $list_years ;//$txt_result ;
}


function listOfClubs($tab_licences) {
	$list_clubs = [] ;
	foreach($tab_licences as $elt_licence) {
		if (in_array($elt_licence[2], $list_clubs) == false) {
			array_push($list_clubs, $elt_licence[2]) ;
		}
	}
	return $list_clubs ; //$txt_result ;
}

function listOfLeagues($tab_licences) {
	$list_leagues = [] ;
	foreach($tab_licences as $elt_licence) {
		if (in_array($elt_licence[3], $list_leagues) == false) {
			array_push($list_leagues, $elt_licence[3]) ;
		}
	}
	return $list_leagues ; //$txt_result ;
}

$licences = [
	["licence_001","2021","75Pa","IDF"],
	["licence_002","2021","75Ju","IDF"],
	["licence_003","2021","38Gr","LRA"],
	["licence_004","2021","38Gr","LRA"],
		
	["licence_005","2022","75Pa","IDF"],
	["licence_004","2022","75Ju","IDF"],
	["licence_003","2022","38Gr","LRA"],
	["licence_002","2022","38Gr","LRA"],
	["licence_001","2022","64Pa","LSO"],
	
	["licence_002","2023","64Pa","LSO"],
	["licence_001","2024","64Pa","LSO"],
	["licence_003","2025","64Pa","LSO"],
	["licence_005","2026","64Pa","LSO"],
	["licence_004","2027","64Pa","LSO"],
	
	["licence_004","2028","38Gr","LRA"]
] ;


echo 'Liste de toutes les licences<br />';
showLicences($licences) ;


echo '<br /><br />Liste des licences :<br />' ;
$liste_des_licences = listOfLicencesNumber($licences) ;
$txt_result_licences = '' ;
$tmp_coma = 0 ;
foreach($liste_des_licences as $tmp_licenceNumber){
	if($tmp_coma > 0) {
		$txt_result_licences .= ', ' ;
	}
	$tmp_coma += 1 ;	
	$txt_result_licences .= $tmp_licenceNumber ;
}
echo $txt_result_licences ;


echo '<br /><br />Liste des millesimes : :<br />' ;
$liste_des_annees = listOfYears($licences) ;
$txt_result_annee = '' ;
$tmp_coma = 0 ;	
foreach($liste_des_annees as $tmp_annee){
	if($tmp_coma > 0) {
		$txt_result_annee .= ', ' ;
	}
	$tmp_coma += 1 ;	
	$txt_result_annee .= $tmp_annee ;
}
print($txt_result_annee) ;


echo '<br /><br />Liste des clubs :<br />' ;
$liste_des_clubs = listOfClubs($licences) ;
$txt_result_club = '' ;
$tmp_coma = 0 ;	
foreach($liste_des_clubs as $tmp_club){
	if($tmp_coma > 0) {
		$txt_result_club .= ', ' ;
	}
	$tmp_coma += 1 ;	
	$txt_result_club .= $tmp_club ;
}
print($txt_result_club) ;



echo '<br /><br />Liste des ligues :<br />' ;
$liste_des_ligues = listOfLeagues($licences) ;
$txt_result_ligues = '' ;
$tmp_coma = 0 ;	
foreach($liste_des_ligues as $tmp_league){
	if($tmp_coma > 0) {
		$txt_result_ligues .= ', ' ;
	}
	$tmp_coma += 1 ;	
	$txt_result_ligues .= $tmp_league ;
}
print($txt_result_ligues) ;


/* ################################################################*/
print("<br /><br />Projection des licences par années/club :<br />") ;

$tab_projection = [] ;

foreach ($liste_des_licences as $lic) {
	$tab_lic = [] ;
	array_push($tab_lic, $lic) ;
	$tmp_list_years = [] ;
	
	foreach($liste_des_annees as $annee) {
		$txt_annee = "" ;
		foreach($licences as $ligne) {
			if (($ligne[0] == $lic) && ($ligne[1] == $annee)) {
 				$txt_annee .= $ligne[2] ;
 			}
 		}
 		array_push($tmp_list_years, $txt_annee) ;
	}
	array_push($tab_lic, $tmp_list_years) ;
	array_push($tab_projection, $tab_lic) ;
}

//array_push($tab_projection, "toto");





$txt_titre =  " | num_licence" ;
$txt_ligne =  " +-----------------" ;

foreach ($liste_des_annees as $elt_annee) {
	$txt_titre .= " | " . $elt_annee  ;
	$txt_ligne .= "-+-------" ;
}

$txt_titre .= " | <br />" ;
$txt_ligne .= "-+" ;

print($txt_titre) ;
print($txt_ligne) ;

print('<br />');
foreach ($tab_projection as $proj) {
	$txt_proj = "| " . $proj[0] ;
	$txt_elt = "" ;
	foreach ($proj[1] as $elt) {
		if ($elt == "") {
			$txt_elt .= " | &nbsp; &nbsp; &nbsp; &nbsp;" ;
		}
		else {
			$txt_elt .= " |&nbsp;&nbsp;" . $elt ;
		}
	}
	$txt_proj .= $txt_elt . " |<br />" ;
	print($txt_proj) ;
}


/* #################################################################*/
print("<br /><br />Projection des clubs par annnée/nombre-de-licencés:<br />") ;


$tab_projection_club = [] ;
foreach ($liste_des_clubs as $elt_club) {
	$tab_stat_club = [] ;
	array_push($tab_stat_club, $elt_club) ;

	$tab_statsDuClub = [] ;
	foreach ($liste_des_annees as $elt_annee) {
		$txt_club_annee = $elt_club . " / " . $elt_annee ; 
		$nb_licences_club_annee = 0 ;
		
		foreach ($licences as $elt_ligne_licence) {
			if($elt_ligne_licence[2] == $elt_club) {
				if($elt_ligne_licence[1] == $elt_annee) { 
					$nb_licences_club_annee += 1 ;
				}
			}		
		}
		array_push($tab_statsDuClub, $nb_licences_club_annee) ;	
	}
	array_push($tab_stat_club, $tab_statsDuClub) ;
	array_push($tab_projection_club, $tab_stat_club) ;
}
#print(tab_projection_club)


$txt_titre  = " | club" ;
$txt_separateur  = " +-----" ;


foreach ($liste_des_annees as $elt_annee) {
	$txt_titre .= " | " . $elt_annee ;
	$txt_separateur .= "-+-------" ;
}

$txt_titre .= " |" ;
$txt_separateur .= "-+" ;

print($txt_titre) ;
print($txt_separateur) ;


foreach ($tab_projection_club as $stat_club) {
	$txt_club = "" ;
	$txt_club .= " |" . $stat_club[0] ;
	
	print('<br />') ;
	foreach ($stat_club[1] as $tmp_stat) {
		//print($tmp_stat) ;
		$txt_club .= "  | &nbsp; &nbsp; &nbsp; &nbsp;" ;
		$txt_club .= $tmp_stat ;
	}
	
	$txt_club .= "  |" ;
	print($txt_club) ;
}
?>
