############################################################################
# https://framagit.org/clement.beni/fedestats/-/tree/FedeStats_Python_0.1/ #
#                                                                          #
# FedeStats v0.1 (Python)                                                  #
############################################################################


def showLicences(tab_licences) :
	for elt_licence in tab_licences :
		print(elt_licence)


def listOfLicencesNumber(tab_licences) :
	list_licenceNumber = []
	for elt_licence in tab_licences :
		if not (elt_licence[0] in list_licenceNumber) :
			list_licenceNumber.append(elt_licence[0])
	return list_licenceNumber


def listOfYears(tab_licences) :
	list_years = []
	for elt_licence in tab_licences :
		if not (elt_licence[1] in list_years) :
			list_years.append(elt_licence[1])
	return list_years


def listOfClubs(tab_licences) :
	list_clubs = []
	for elt_licence in tab_licences :
		if not (elt_licence[2] in list_clubs) :
			list_clubs.append(elt_licence[2])
	return list_clubs


def listOfLeagues(tab_licences) :
	list_leagues = []
	for elt_licence in tab_licences :
		if not (elt_licence[3] in list_leagues) :
			list_leagues.append(elt_licence[3])
	return list_leagues

###################################################################
###################################################################

licences = [
	["licence_001","2021","75Pa","IDF"],
	["licence_002","2021","75Ju","IDF"],
	["licence_003","2021","38Gr","LRA"],
	["licence_004","2021","38Gr","LRA"],
		
	["licence_005","2022","75Pa","IDF"],
	["licence_004","2022","75Ju","IDF"],
	["licence_003","2022","38Gr","LRA"],
	["licence_002","2022","38Gr","LRA"],
	["licence_001","2022","64Pa","LSO"],
	
	["licence_002","2023","64Pa","LSO"],
	["licence_001","2024","64Pa","LSO"],
	["licence_003","2025","64Pa","LSO"],
	["licence_005","2026","64Pa","LSO"],
	["licence_004","2027","64Pa","LSO"],
	
	["licence_004","2028","38Gr","LRA"]
]


print("Liste de toutes les licences")	
showLicences(licences)


print("\nListe des licences :")	
liste_des_licences = listOfLicencesNumber(licences)
print(liste_des_licences)


print("\nListe des millesimes :")
liste_des_annees = listOfYears(licences)
print(liste_des_annees)


print("\nListe des clubs :")	
liste_des_clubs = listOfClubs(licences)
print(liste_des_clubs)


print("\nListe des ligues :")	
liste_des_ligues = listOfLeagues(licences)
print(liste_des_ligues)


################################################################
print("\nProjection des licences par années/club :")

tab_projection = []

for lic in liste_des_licences :
	tab_lic = []
	tab_lic.append(lic)
	tmp_list_years = []
	for annee in liste_des_annees :
		txt_annee = ""
		for ligne in licences :
			if (ligne[0] == lic) and (ligne[1] == annee) :
 				txt_annee += ligne[2]
		tmp_list_years.append(txt_annee)
	tab_lic.append(tmp_list_years)
	tab_projection.append(tab_lic)
	

txt_titre =  " | num_licence"
txt_ligne =  " +------------"

for elt_annee in liste_des_annees :
	txt_titre += " | " + elt_annee  
	txt_ligne += "-+-----"

txt_titre += " |"
txt_ligne += "-+"

print(txt_titre)
print(txt_ligne)

for proj in tab_projection :
	txt_proj = " | " + proj[0]
	txt_elt = ""
	for elt in proj[1] :
		if (elt == "") :
			txt_elt += " |     "
		else :
			txt_elt += " | " + elt
	txt_proj += txt_elt + " |"
	print(txt_proj)





#################################################################
print("\n\nProjection des clubs par annnée/nombre-de-licencés:")

tab_projection_club = []
for elt_club in liste_des_clubs :
	tab_stat_club = []
	tab_stat_club.append(elt_club)

	tab_statsDuClub = []
	for elt_annee in liste_des_annees :
		txt_club_annee = elt_club + " / " + elt_annee 
		nb_licences_club_annee = 0
		
		for elt_ligne_licence in licences :
			if(elt_ligne_licence[2] == elt_club) :
				if(elt_ligne_licence[1] == elt_annee) : 
					nb_licences_club_annee += 1		
		tab_statsDuClub.append(nb_licences_club_annee)		
	
	tab_stat_club.append(tab_statsDuClub)
	tab_projection_club.append(tab_stat_club)

#print(tab_projection_club)


txt_titre  = " | club"
txt_separateur  = " +-----"

for elt_annee in liste_des_annees :
	txt_titre += " | " + elt_annee
	txt_separateur += "-+-----"
	
txt_titre += " |"
txt_separateur += "-+"

print(txt_titre)
print(txt_separateur)

for stat_club in tab_projection_club :
	txt_club = ""
	txt_club += " |" + str(stat_club[0])
	
	for tmp_stat in stat_club[1] :
		#print(tmp_stat)
		txt_club += "  |   "
		txt_club += str(tmp_stat)
	txt_club += "  |"
	print(txt_club)






